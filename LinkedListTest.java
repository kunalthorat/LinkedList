class LinkedList {
  private Node head;

  public void addData(int data) {
    if (head == null) {
      head = new Node(data);
      head.next = null;
    } else {
      Node temp = new Node(data);
      Node current = head;
      // System.out.println(current.getData());
      if (current != null) {
        while (current.getNext() != null) {

          // System.out.println(current.getData());
          current = current.getNext();
          // System.out.println("Current is: "+current.getData());
        }

        current.setNext(temp);
      }
    }
  }

  public void showLinkedList() {
    if (head == null) {
      System.out.println("Empty list.....");
    } else {
      System.out.printf("Printing List......\n");
      System.out.println();
      Node current = head;
      while (current.getNext() != null) {
        System.out.println(current.getData());
        current = current.getNext();
        if(current.getNext() == null) {
          System.out.println(current.getData());
        }
      }
    }
  }

  private class Node {
    int data;
    Node next;

    Node(int value) {
      data = value;
      next = null;
    }

    int getData() {
      return data;
    }

    public Node getNext() {
      return next;
    }

    public void setNext(Node nextNode) {
      next = nextNode;
    }
  }
}

public class LinkedListTest {
  public static void main(String[] args) {
    System.out.println("Initialized");

    LinkedList linkedList = new LinkedList();
    for (int i = 1; i <= 10; i++) {
      linkedList.addData(i);
    }
    linkedList.showLinkedList();
  }
}
